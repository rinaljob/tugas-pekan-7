<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

Route::group(['middleware' => ['auth']], function () {
    
Route::get('/home', 'IndexController@index');

Route::get('/pendaftaran', 'AuthController@bio');

Route::post('/welcome', 'AuthController@kirim');

Route::get('/data-table', function(){
    return view('table.data-table');
});

// CRUD Cast
// Create
Route::get('/cast','CastController@create');
Route::get('/cast/create','CastController@create');
Route::post('/cast','CastController@store');
// Read
Route::get('/cast','CastController@index');
Route::get('/cast/{cast_id}','CastController@show');
// Update
Route::get('/cast/{cast_id}/edit','CastController@edit');
Route::put('/cast/{cast_id}','CastController@update');
// Delete
Route::delete('/cast/{cast_id}','CastController@destroy');

});

Auth::routes();




