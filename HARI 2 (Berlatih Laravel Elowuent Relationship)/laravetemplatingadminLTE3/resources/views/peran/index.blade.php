@extends('layout.master')

@section('judul')
DAFTAR LIST PERAN
@endsection
@section('content')

<div class="row">
    @forelse ($peran as $item)
        <div class="col-4">
            <div class="card">
                <div class="card-body">
                <h3>{{$item->judul}}</h3>
                <p class="card-text"> {{Str::limit($item->ringkasan, 30)}} </p>
                <a href="/peran/{{$item->id}}" class="btn btn-info btn-info-sm">Detail</a>
                <a href="#" class="btn btn-warning btn-info-sm">Edit</a>
                <a href="#" class="btn btn-danger btn-info-sm">Delete</a>
            </div>
        </div>
        </div>
     </div>
     @empty
     <h4>Data Peran Belum Ada</h4>
    @endforelse
    <a href="/film/create" class="btn btn-secondary mt-3 mb-3">Tambah Film</a>
</div>

@endsection