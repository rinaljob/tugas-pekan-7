@extends('layout.master')

@section('judul')
HALAMAN DETIL PERAN {{$film->judul}}
@endsection

@section('content')
<h1>{{$peran->judul}}</h1>
<p>{{$peran->content}}</p>

<h1>Kritik</h1>
<form action="/peran" method="post" enctype="multipart/form-data" class=",y-3">
    @csrf
      <div class="form-group">
        <label>Content</label>
        <input type="hidden" name="film_id" value="{{$peran->id}}">
        <textarea name="isi" class="form-control" cols="30" rows="10"></textarea>
    </div>
      
      @error('isi')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
  
<a href="/peran" class="btn btn=secondary">Kembali</a>

@endsection