@extends('layout.master')

@section('judul')
Tambah Genre
@endsection
@section('content')
<form action="/genre" method="post">
  @csrf
    <div class="form-group">
      <label>Nama Genre</label>
      <input type="text" name="nama" class="form-control">
    </div>
    
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Tambah</button>
    <a href="/genre/create" class="btn btn-danger mt-3 mb-3">Batal</a>
  </form>

  @endsection