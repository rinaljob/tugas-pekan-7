@extends('layout.master')

@section('judul')
Edit Pemain Film
@endsection
@section('content')

<form action="/genre/{{$genre->id}}" method="post">
    @csrf
    @method('PUT')
      <div class="form-group">
        <label>Nama Genre</label>
        <input type="text" name="nama" value="{{$genre->nama}}" class="form-control">
      </div>
      
      @error('nama')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
  
      <button type="submit" class="btn btn-primary">Ubah</button>
    </form>

@endsection