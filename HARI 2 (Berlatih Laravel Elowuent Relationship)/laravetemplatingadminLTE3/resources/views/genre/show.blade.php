@extends('layout.master')

@section('judul')
Detil Cast
@endsection
@section('content')

<h1>{{$genre->nama}}</h1>

<h1>Genre</h1>
<form action="/genre" method="post" enctype="multipart/form-data" class=",y-3">
    @csrf
      <div class="form-group">
        <label>Genre</label>
        <input type="hidden" name="genre_id" value="{{$genre->id}}">
        <textarea name="isi" class="form-control" cols="30" rows="10"></textarea>
    </div>
      
      @error('isi')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
  
      <a href="/genre" class="btn btn-secondary mt-3 mb-3">Kembali</a>
  

@endsection