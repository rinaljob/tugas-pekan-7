@extends('layout.master')

@section('judul')
Daftar Genre
@endsection
@section('content')


<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($genre as $key => $item)    
<tr>
    <td>{{$key + 1}}</td>
    <td>{{$item->nama}}</td>
    <td>
        <form action="/genre/{{$item->id}}" method="POST">
        <a href="/genre/{{$item->id}}" class="btn btn-success btn-sm">Detail</a>
        <a href="/genre/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
        @csrf
        @method('delete')
        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
        </form>

    </td>
</tr>
        @empty
        <h1>Data tidak ada</h1>
        @endforelse
    </tbody>
  
  </table>
  <a href="/genre/create" class="btn btn-secondary mt-3 mb-3">Tambah Genre</a>


@endsection