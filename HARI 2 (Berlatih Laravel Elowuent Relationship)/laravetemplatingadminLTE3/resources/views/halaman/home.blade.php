
@extends('layout.master')

@section('judul')
Halaman home
@endsection

@section('content')
    <h1>SELAMAT DATANG</h1>
    <h3>Sosial Media Developer</h3>
    <p>Belajar dan berbagi agar hidup menjadi berkah</p>

    <h3>Keuntungan join di Media Offline</h3>
    <ul>
        <li>Mendapatkan Motivasi dari sesama para Developer</li>
        <li>Sharing Knowledge</li>
        <li>Dibuat oleh calon developer terbaik</li>
    </ul>
    
    <h3>Cara gabung ke Media Offline</h3>
    <ol>
        <li>Mengunjungi Website ini</li>
        <li>Mendaftarkan di <a href="{{url('/pendaftaran')}}"><strong>Form Pendaftaran</strong></a>
        <li>Selesai</li>
    </ol>
    
@endsection