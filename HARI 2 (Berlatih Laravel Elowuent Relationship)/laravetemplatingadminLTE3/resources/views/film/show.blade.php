@extends('layout.master')

@section('judul')
HALAMAN DETIL FILM {{$film->judul}}
@endsection

@section('content')

<img src="{{asset('gambar/'.$item->poster)}}" class="card-img-top" alt="...">
<h1>{{$film->judul}}</h1>
<p>{{$film->content}}</p>

<h1>Kritik</h1>
<form action="/cast" method="post" enctype="multipart/form-data" class=",y-3">
    @csrf
      <div class="form-group">
        <label>Content</label>
        <input type="hidden" name="film_id" value="{{$cast->id}}">
        <textarea name="isi" class="form-control" cols="30" rows="10"></textarea>
    </div>
      
      @error('isi')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
  
<a href="/film" class="btn btn=secondary">Kembali</a>

@endsection