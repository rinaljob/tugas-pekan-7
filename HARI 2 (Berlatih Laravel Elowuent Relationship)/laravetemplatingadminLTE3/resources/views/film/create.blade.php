@extends('layout.master')

@section('judul')
Tambah FILM
@endsection
@section('content')
<form action="/film" method="post" enctype="multipart/form-data">
  @csrf
    <div class="form-group">
      <label>Judul Film</label>
      <input type="text" name="judul" class="form-control">
    </div>
    
    @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Ringkasan</label>
      <textarea name="ringkasan" class="form-control" cols="30" rows="10"></textarea>
    </div>
    
    @error('ringkasan')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
      <label>Tahun</label>
      <input type="number" name="tahun" class="form-control">
    </div>

    @error('tahun')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
      <label>Genre</label>
      <select name="genre_id" class="form-control" id="">
        <option value="">---Pilih Genre---</option>
        @foreach ($genre as $item)
            <option value="{{$item->id}}">{{$item->nama}}</option>
        @endforeach
      </select>
    </div>

    <div class="form-group">
        <label>Poster</label>
        <input type="file" name="poster" class="form-control">
      </div>
  
      @error('poster')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      

    @error('tahun')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Tambah</button>
    <a href="/cast/create" class="btn btn-danger mt-3 mb-3">Batal</a>
  </form>

  @endsection