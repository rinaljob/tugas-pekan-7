@extends('layout.master')

@section('judul')
Detil Cast
@endsection
@section('content')

<h1>{{$cast->nama}}</h1>
<p>{{$cast->umur}}</p>
<p>{{$cast->bio}}</p>

<a href="/cast" class="btn btn-secondary mt-3 mb-3">Kembali</a>  

@endsection