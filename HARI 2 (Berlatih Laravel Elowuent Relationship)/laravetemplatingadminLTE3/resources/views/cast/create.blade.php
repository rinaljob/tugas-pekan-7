@extends('layout.master')

@section('judul')
Tambah Pemain Film
@endsection
@section('content')
<form action="/cast" method="post">
  @csrf
    <div class="form-group">
      <label>Nama</label>
      <input type="text" name="nama" class="form-control">
    </div>
    
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Umur</label>
      <input type="text" name="umur" class="form-control">
    </div>
    
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
      <label>Bio</label>
      <input type="umur" name="bio" class="form-control">
    </div>

    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Tambah</button>
    <a href="/cast/create" class="btn btn-danger mt-3 mb-3">Batal</a>
  </form>

  @endsection