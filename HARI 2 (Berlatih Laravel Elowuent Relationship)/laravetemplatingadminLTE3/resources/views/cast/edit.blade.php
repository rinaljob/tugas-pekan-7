@extends('layout.master')

@section('judul')
Edit Pemain Film
@endsection
@section('content')

<form action="/cast/{{$cast->id}}" method="post">
    @csrf
    @method('PUT')
      <div class="form-group">
        <label>Nama Cast</label>
        <input type="text" name="nama" value="{{$cast->nama}}" class="form-control">
      </div>
      
      @error('nama')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
  
      <div class="form-group">
        <label>Umur</label>
        <input type="text" name="umur" value="{{$cast->umur}}" class="form-control">
      </div>
      
      @error('umur')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      
      <div class="form-group">
        <label>Bio</label>
        <textarea name="bio" class="form-control">{{$cast->bio}}</textarea>
      </div>
  
      @error('bio')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
  
      <button type="submit" class="btn btn-primary">Ubah</button>
      <a href="/cast" class="btn btn-secondary mt-3 mb-3">Kembali</a>
    </form>

@endsection