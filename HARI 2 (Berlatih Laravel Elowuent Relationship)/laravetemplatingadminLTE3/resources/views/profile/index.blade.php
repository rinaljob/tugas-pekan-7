@extends('layout.master')

@section('judul')
EDIT PROFILE
@endsection
@section('content')

<form action="/profile/{{$profile->id}}" method="post">
    @csrf
    @method('PUT')
      <div class="form-group">
        <label>Nama User</label>
        <input type="text" value="{{$profile->user->name}}" class="form-control" disabled>
      </div>
      <div class="form-group">
        <label>Email User</label>
        <input type="text" value="{{$profile->user->email}}" class="form-control" disabled>
      </div>

      <div class="form-group">
        <label>Umur Profile</label>
        <input type="number" name="umur" value="{{$profile->umur}}" class="form-control">
      </div>
      
      @error('umur')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
  
      <div class="form-group">
        <label>Biodata</label>
        <textarea name="bio" class="form-control" cols="30" rows="10">{{$profile->bio}}</textarea>
      </div>
      
      @error('bio')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      
      
      <div class="form-group">
        <label>Alamat</label>
        <textarea name="alamat" class="form-control">{{$profile->alamat}}</textarea>
      </div>
  
      @error('alamat')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
  
      <button type="submit" class="btn btn-primary">Ganti</button>
    </form>

@endsection