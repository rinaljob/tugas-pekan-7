<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kritik extends Model
{
    protected $table = 'kritik';

    protected $fillable = ['user_id','film_id','content','point'];
}
