<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function bio(){
        return view('halaman.pendaftaran');
    }

    public function kirim(Request $request){
        $namadpn = $request['namadpn'];
        $namablk = $request['namablk'];
        return view('halaman.welcome', compact('namadpn','namablk'));
    }
}

