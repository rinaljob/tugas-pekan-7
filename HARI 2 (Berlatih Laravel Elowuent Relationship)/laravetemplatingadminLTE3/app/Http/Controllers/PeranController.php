<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Peran;

class PeranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $peran = Peran::all();

        return view('peran.index',compact('peran'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'genre_id' => 'required',
            'poster' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            
        ]);

        $poster = time().'.'.$request->poster->extension();

        $request->poster->move(public_path('gambar'),$poster);

      

        $film = new Film;

        $film->judul = $request->judul;
        $film->ringkasan = $request->ringkasan;
        $film->tahun = $request->tahun;
        $film->genre_id = $request->genre_id;
        $film->poster = $request->poster;

        $film->save();

        return redirect('/film/create');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $peran = Peran::where('id', $peran_id)->first();
        return view('peran.show', compact('peran'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'film_id' => 'required',
            'cast_id' => 'required',
            'nama' => 'required',
        ]);

        $peran = Peran::find($peran_id);
        
        $peran->film_id = $request->film_id;
        $peran->cast_id = $request->cast_id;
        $peran->nama = $request->nama;
        
        $peran->save();
        return redirect('/film');
        }

        public function destroy($film_id)
        {
           //
        }
    }

