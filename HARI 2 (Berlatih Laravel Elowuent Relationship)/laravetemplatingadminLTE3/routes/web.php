<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'IndexController@index');


Route::get('/home', 'IndexController@index');

Route::get('/pendaftaran', 'AuthController@bio');

Route::post('/welcome', 'AuthController@kirim');

Route::get('/data-table', function(){
    return view('table.data-table');
});


Route::group(['middleware' => ['auth']], function () {
// CRUD Cast
// Create
Route::get('/cast','CastController@create');
Route::get('/cast/create','CastController@create');
Route::post('/cast','CastController@store');
// Read
Route::get('/cast','CastController@index');
Route::get('/cast/{cast_id}','CastController@show');
// Update
Route::get('/cast/{cast_id}/edit','CastController@edit');
Route::put('/cast/{cast_id}','CastController@update');
// Delete
Route::delete('/cast/{cast_id}','CastController@destroy');

// CRUD Genre
// Create
Route::get('/genre','GenreController@create');
Route::get('/genre/create','GenreController@create');
Route::post('/genre','GenreController@store');
// Read
Route::get('/genre','GenreController@index');
Route::get('/genre/{genre_id}','GenreController@show');
// Update
Route::get('/genre/{genre_id}/edit','GenreController@edit');
Route::put('/genre/{genre_id}','GenreController@update');
// Delete
Route::delete('/genre/{genre_id}','GenreController@destroy');

// CRUD Peran
// Create
Route::get('/peran','PeranController@create');
Route::get('/peran/create','PeranController@create');
Route::post('/peran','PeranController@store');
// Read
Route::get('/peran','PeranController@index');
Route::get('/peran/{peran_id}','PeranController@show');
// Update
Route::get('/peran/{peran_id}/edit','PeranController@edit');
Route::put('/peran/{peran_id}','PeranController@update');
// Delete
Route::delete('/peran/{peran_id}','PeranController@destroy');

// Update Profile
Route::resource('profile','ProfileController')->only([
    'index','update']);
});


// CRUD FILM
Route::resource('film','FilmController');


Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
